/* ============== Dependencias ============== */
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

/* ============== Instancia de servidor ============== */
const app = express();

/* ============== Middlewares ============== */
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

/* ============== Puerto ============== */
app.set('port', 3030);
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});

/* ============== Rutas gestionadas ==============*/
require('./services/tipos/tipos.routes')(app);
require('./services/movimientos/movimientos.routes')(app);