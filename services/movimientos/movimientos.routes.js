const movimientosModel = require('./movimientos.model');

module.exports = (app) => {
    app.get('/movimientos', (req, res) => {
        let parametros = {}

        movimientosModel.obtenerMovimientos(parametros,  (error, resultado) => {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`
                });
            } else {
                res.status(200).json({
                    codigo: 200,
                    resultado: resultado
                });
            }
        });
    });

    app.get('/movimientos/:id_movimiento', (req, res) => {
        let parametros = {
            id_movimiento: req.params.id_movimiento 
        }

        movimientosModel.obtenerMovimiento(parametros,  (error, resultado) => {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`
                });
            } else {
                res.status(200).json({
                    codigo: 200,
                    resultado: resultado
                });
            }
        });
    });
}