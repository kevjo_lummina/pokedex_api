const conexion = require('../../conexion');
const movimientosHooks = require('./movimientos.hooks');

var movimientosModel = {}

//PETICION GET
movimientosModel.obtenerMovimientos = (parametros, callback) => {
    let consulta = `SELECT 	A.id_movimiento, 
                            A.movimiento,
                            A.id_tipo,
                            B.tipo,
                            A.id_categoria,
                            C.categoria
                    FROM db_pokedex.tbl_movimientos as A
                    INNER JOIN db_pokedex.tbl_tipos as B
                    ON (A.id_tipo = B.id_tipo)
                    INNER JOIN db_pokedex.tbl_categorias as C
                    ON (A.id_categoria = C.id_categoria);`;

    conexion.query(
        consulta,
        (error, resultado, registros) => {
            if (error) {
                callback(error, null);
            } else {
                // movimientosHooks.postConsulta(resultado, 'tipo', ['id_tipo', 'tipo']);
                callback(null, resultado);
            }        
        }
    );
}

movimientosModel.obtenerMovimiento = (parametros, callback) => {
    let consulta = `SELECT 	A.id_movimiento, 
                            A.movimiento,
                            A.id_tipo,
                            B.tipo,
                            A.id_categoria,
                            C.categoria
                    FROM db_pokedex.tbl_movimientos as A
                    INNER JOIN db_pokedex.tbl_tipos as B
                    ON (A.id_tipo = B.id_tipo)
                    INNER JOIN db_pokedex.tbl_categorias as C
                    ON (A.id_categoria = C.id_categoria)
                    WHERE A.id_movimiento = ?;`;

    conexion.query(
        consulta,
        [parametros.id_movimiento],
        (error, resultado, registros) => {
            if (error) {
                callback(error, null);
            } else {
                callback(null, resultado);
            }        
        }
    );
}

module.exports = movimientosModel;