const conexion = require('../../conexion');

var tiposModel = {}

//PETICION GET
tiposModel.obtenerTipos = (parametros, callback) => {
    let consulta = `SELECT 	id_tipo, 
                            tipo
                        FROM db_pokedex.tbl_tipos;`;

    conexion.query(
        consulta,
        (error, resultado, registros) => {
            if (error) {
                callback(error, null);
            } else {
                callback(null, resultado);
            }        
        }
    );
}

tiposModel.obtenerTipo = (parametros, callback) => {
    let consulta = `SELECT 	id_tipo, 
                            tipo
                    FROM db_pokedex.tbl_tipos
                    WHERE id_tipo = ?;`;

    conexion.query(
        consulta,
        [parametros.id_tipo],
        (error, resultado, registros) => {
            if (error) {
                callback(error, null);
            } else {
                callback(null, resultado);
            }        
        }
    );
}

module.exports = tiposModel;