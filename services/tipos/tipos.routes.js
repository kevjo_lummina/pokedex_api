const tiposModel = require('./tipos.model');

module.exports = (app) => {
    app.get('/tipos_elementales', (req, res) => {
        let parametros = {}

        tiposModel.obtenerTipos(parametros,  (error, resultado) => {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`
                });
            } else {
                res.status(200).json({
                    codigo: 200,
                    resultado: resultado
                });
            }
        });
    });

    app.get('/tipos_elementales/:id_tipo', (req, res) => {
        let parametros = {
            id_tipo: req.params.id_tipo  
        }

        tiposModel.obtenerTipo(parametros,  (error, resultado) => {
            if (error) {
                res.status(500).json({
                    codigo: error.errno,
                    mensaje: `${error.code} ${error.sqlMessage}`
                });
            } else {
                res.status(200).json({
                    codigo: 200,
                    resultado: resultado
                });
            }
        });
    });
}